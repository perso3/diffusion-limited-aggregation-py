import pygame


def clear(window):
    window.fill((0, 0, 0))


def render(window, walkers):
    clear(window)
    for walker in walkers:
        if walker.fixed:
            color = (255, 0, 0)
        else:
            color = (255, 255, 255)

        pos = walker.pos.getPos()
        r = walker.size

        pygame.draw.circle(window, color, pos, r)
