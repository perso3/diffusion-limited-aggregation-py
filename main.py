import random

import pygame
from pygame.locals import *
import time
from BasicVector import Vec2
import random

from walker import Walker
from renderer import *


def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("")
    mainClock = pygame.time.Clock()

    simulation = True

    walkers = []

    for i in range(0, 360):
        orientation = Vec2.degreesToVec2(i) * 270
        new_pos = Vec2(300, 300) + orientation
        walkers.append(Walker(new_pos, 10, 5, 1, True))

    for i in range(200):
        new_pos = Vec2(300, 300)
        walkers.append(Walker(new_pos, 5, 5, 1))

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pass

        if not pause:
            pass

        if frameCount % 1 == 0:
            for walker in walkers:
                if not walker.fixed:
                    walker.update(walkers)
                    if walker.fixed:
                        new_pos = Vec2(300, 300)
                        walkers.append(Walker(new_pos, 5, 5, 1))

            render(window, walkers)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()