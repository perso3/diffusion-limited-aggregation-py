from BasicVector import Vec2
import random


class Walker:

    def __init__(self, pos, size, step, stickiness, fixed=False):
        self.pos = pos
        self.orientation = random.randrange(0, 360)
        self.size = size
        self.step = step
        self.fixed = fixed
        self.stickiness = stickiness

    def reset(self, pos):
        self.pos = pos
        self.orientation = Vec2.vec2ToDegrees(Vec2(300, 300) - self.pos)

    def update(self, walkers):
        self.orientation += random.randrange(-30, 30)
        direction = Vec2.degreesToVec2(self.orientation)
        self.pos += direction * self.step
        for walker in walkers:
            if walker.fixed:
                if self.dist(walker) <= self.size**2 + walker.size**2:
                    self.fixed = True

    def dist(self, o):
        x = (self.pos.x - o.pos.x)
        y = (self.pos.y - o.pos.y)

        return x**2 + y**2
